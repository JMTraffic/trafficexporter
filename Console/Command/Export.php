<?php

namespace Traffic\Exporter\Console\Command;

use Magento\Framework\App\ObjectManagerFactory;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Export extends Command
{
    /**
     * @var string
     */
    protected $entityCode;

    /**
     * @var object
     */
    protected $input;
    /**
     * @var object
     */
    protected $output;

    /**
     * Object manager factory.
     *
     * @var ObjectManagerFactory
     */
    private $objectManagerFactory;
    /**
     * Constructor.
     *
     * @param ObjectManagerFactory $objectManagerFactory
     */
    public function __construct(ObjectManagerFactory $objectManagerFactory)
    {
        $this->objectManagerFactory = $objectManagerFactory;
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;

        $omParams = $_SERVER;
        $omParams[StoreManager::PARAM_RUN_CODE] = 'admin';
        $omParams[Store::CUSTOM_ENTRY_POINT_PARAM] = true;
        $this->objectManager = $this->objectManagerFactory->create($omParams);

        $exporter = $this->objectManager->get('Traffic\Exporter\Model\Export');
        $writer = $this->objectManager->get('Magento\ImportExport\Model\Export\Adapter\Csv');
        $exporter->setWriter($writer);

        if (!is_dir('var/export')) {
            mkdir('var/export');
        }
        file_put_contents('var/export/export.csv', $exporter->export());
    }

    protected function configure()
    {
        $this->setName('traffic:export:products')
            ->setDescription('Export Products');
        $this->setEntityCode('catalog_product');
        parent::configure();
    }

    /**
     * @return string
     */
    public function getEntityCode()
    {
        return $this->entityCode;
    }
    /**
     * @param string $entityCode
     */
    public function setEntityCode($entityCode)
    {
        $this->entityCode = $entityCode;
    }
}
